import Vue from 'vue'
import Vuex from "vuex";
import productStore  from "./modules/products";

Vue.use(Vuex);

const store =  new Vuex.Store({
  modules: {
    products:productStore
  },
  state:{
    count: 0
  },
  mutations:{
    incrementCount(state){
      state.count ++
    },
    decrementCount(state){
      state.count --
    }
  }
})

export default store