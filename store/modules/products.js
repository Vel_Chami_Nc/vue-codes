import { plainInstance as plain } from '../../server/index.js' 

const productStore = {
	namespaced: true,
	state:{
		products: []
	},
	mutations:{
		setProducts(state, products){
			state.products = products
		}
	},
	actions:{
		index( {commit} ){
			return new Promise((resolve, reject) =>{
				plain.get('products')
				.then(response =>{
					commit('setProducts', response.data)
					resolve(response)
				})
				.catch(error =>{
					console.log(error)
					reject(error)
				})
			})
		}
	}
}

export default productStore