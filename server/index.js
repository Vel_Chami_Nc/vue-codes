import axios from 'axios'
    
const plainInstance = axios.create({  
  baseURL: `https://immense-eyrie-91363.herokuapp.com/`,
  withCredentials: false, // This is the default
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
})

export { plainInstance }