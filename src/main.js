import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store  from '../store/index'

import { plainInstance } from "../server/index"
import VueAxios from 'vue-axios'

Vue.use(VueAxios, {
  plain: plainInstance
});

new Vue({
  router,
  plainInstance,
  store,
  el: "#app",
  components: { App },
  render: h => h(App)
});


