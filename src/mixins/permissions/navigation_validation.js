export default{
	methods:{
		canUserAccess(name){
			let permissions = JSON.parse(localStorage.getItem('permissions'))
			if(permissions && permissions.length > 0){
				let rules = []
				permissions.map(p => { rules.push(p.rules) })
				rules = rules.flat()
				let rule =  rules.find(rule => rule.pageName == name)
				if(rule){
					return rule.status
				}
				return false
			}
			return false
		},
		checkPageAvailable(name){
			let permissions = JSON.parse(localStorage.getItem('permissions'))
			if(permissions && permissions.length > 0){
				let rules = []
				permissions.map(p => { rules.push(p.rules) })
				rules = rules.flat()
				let rule =  rules.find(rule => rule.pageName == name)
				if(rule){
					return true
				}
				return false
			}
			return false
		}
	}
}