export default{
	methods:{
		canAccess(name, action){
      let page = this.permissions.find(p => p.name == name)
      if(page){
        let rule = page.rules.find(r => r.action == action)
        return rule.status
      }
      return false
    }
	}
}