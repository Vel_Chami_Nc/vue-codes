export default{
	data(){
		return{
			permissions:[
				{
					name: 'About',
					rules:[
						{
							action: 'Index',
							status: false,
							pageName: 'About'
						}
					]
				},
				{
					name: 'Product',
					rules:[
						{
							action: 'Index',
							status: true,
							pageName: 'Products'
						},
						{
							action: 'Show',
							status: true,
							pageName: 'Product'
						},
						{
							action: 'New',
							status: true,
							pageName: 'newProduct'
						},
						{
							action: 'Update',
							status: true,
							pageName: 'editProduct'
						},
						{
							action: 'Destroy',
							status: false,
							pageName: 'deleteProduct'
						}
					]
				},
				{
					name: 'Permissions',
					rules:[
						{
							action: 'Index',
							status: true,
							pageName: 'Permissions'
						}
					]
				}
			]
		}
	}
}