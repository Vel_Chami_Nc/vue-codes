import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'
import RouterValidation from "@/mixins/permissions/navigation_validation.js"

const canUserAccess = RouterValidation.methods.canUserAccess
const checkPageAvailable = RouterValidation.methods.checkPageAvailable

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  base: import.meta.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'Products',
      component: HomeView
    },
    {
      path: '/about',
      name: 'About',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/AboutView.vue')
    },
    {
      path: '/new_product',
      name: 'newProduct', 
      component: () => import('../views/NewProduct.vue')
    },
    {
      path: '/products/:id',
      name: 'Product', 
      component: () => import('../views/ProductDetail.vue')
    },
    {
      path: '/products/:id/edit',
      name: 'editProduct', 
      component: () => import('../views/editProduct.vue')
    },
    {
      path: '/permissions',
      name: 'Permissions', 
      component: () => import('../views/Permissions.vue')
    },
    {
      path: '/404',
      name: '404',
      meta: { title: `Page not found` },
      component: () => import('@/views/Error404.vue')

    }
  ]
})

router.beforeEach(async (to, from, next) => {
  if(to.path !== '/404'){
    const canAccess = await canUserAccess(to.name)
    const isPageAvailable = await checkPageAvailable(to.name)
    if(!isPageAvailable){
      next( { path: `/404?pageName=${to.path}`} )
    }
    else if(canAccess){
      next()
    }
    else{
      const canAccess = await canUserAccess('Products')
      if(canAccess){
        next( { path: '/' } )
      }
      else{
      next( { path: '/404' } )
      }
    }
  }
  else{
    next()
  }
})

export default router
